import 'package:flutter/material.dart';

// Application start here !!!
void main() {
  runApp(MaterialApp(
    home: Home(),
  ));
}

// Create a costume Widget
class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        title: Text(
          'my first flutter app',
          style: TextStyle(
            fontFamily: 'Comfortaa',
          ),),
        centerTitle: false,
        backgroundColor: Colors.teal,
      ),
      body: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              MyContainer('I !'),
              MyContainer('My !'),
              MyContainer('Me !'),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              MyContainer('Hello !'),
              MyContainer('Hi !'),
              MyContainer('Bye Bye !'),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  padding: EdgeInsets.all(30.0),
                  color: Colors.amber,
                  child: Image.asset('utility/assets/images/profile.jpg'),
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(
                  padding: EdgeInsets.all(30.0),
                  color: Colors.teal,
                  child: Image.asset('utility/assets/images/cover.jpg'),
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(
                  padding: EdgeInsets.all(30.0),
                  color: Colors.cyan,
                  child: Image.asset('utility/assets/images/profile.jpg'),
                ),
              ),
            ],
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.add_alert,
          color: Colors.white,
          size: 30.0,
        ),
        backgroundColor: Colors.teal,
        onPressed: () {},
      ),
    );
  }
}

class MyContainer extends StatelessWidget {

  String text;

  MyContainer(String text) {
    this.text = text;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
      color: Colors.grey[400],
      child: Text(
        this.text,
        style: TextStyle(
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
            fontFamily: 'Comfortaa',
            letterSpacing: 2.0,
            color: Colors.grey[600]
        ),
      ),
    );
  }
}